//
//  FrequencyDomainMetrics.swift
//  MoreBeta
//

import Foundation
import Accelerate


func argsort<T:Comparable>(_ data : [T] ) -> [Int] {
   return Array(data.indices).sorted{return data[$0] < data[$1]}
}

func normalize(_ data : [Double] ) -> [Double] {
   let mean: Double = data.reduce(0, +) / Double(data.count)
   return data.map{return $0 - mean}
}

func square(_ num: Double) -> Double {
    return pow(num, Double(2))
}


internal class FrequencyDomainMetrics {
    // Source: https://codereview.stackexchange.com/q/154036
    internal func dft(_ data: [Double]) -> ([Double],[Double]) {
        let N = data.count
        var Xre: [Double] = Array(repeating:0, count:N)
        var Xim: [Double] = Array(repeating:0, count:N)
        
        for k in 0..<N
        {
            Xre[k] = 0
            Xim[k] = 0
            for n in 0..<N {
                let q = (Double(n) * Double(k) * 2.0 * Double.pi) / Double(N)
                Xre[k] += data[n] * cos(q) // Real part of X[k]
                Xim[k] -= data[n] * sin(q) // Imag part of X[k]
            }
        }
        return (Xre, Xim)
    }
    
    internal func get_normalized_magnitudes(_ Xre: [Double], _ Xim: [Double]) -> [Double] {
        var results: [Double] = []
        
        for idx in 0..<Xre.count {
            results.append(sqrt(square(Xre[idx]) + square(Xim[idx])) / Double(Xre.count))
        }
        
        return results
    }
    
    internal func fftfreq(_ window_length: Int, _ seconds_between_sample: Double) -> [Double] {
        let rad_hz: Double = 1.0 / (Double(window_length) * seconds_between_sample)
        let N: Int = (window_length - 1) / 2 + 1
        var results: [Int]

        let p1: [Int] = Array(stride(from:0, to:N, by:1))
        let p2: [Int] = Array(stride(from:-(window_length / 2), to:0, by:1))

        results = p1 + p2
        
        return results.map{Double($0) * rad_hz}
    }
    
    private func orderPSD(_ indices: [Int], _ psd: [Double]) -> [Double] {
        var ordered: [Double] = []
        
        for val in indices {
            ordered.append(psd[val])
        }
        
        return ordered
    }
    
    /* input: An array of RR intervals.
       Returns 3 frequencies bands of the ordered PSD.
        HF: 0.15 -> 0.4
        LF: 0.04 -> 0.15
        VLF: 0.0033 -> 0.04
    */
    internal func getFreqBands(_ input: [Double]) -> ([Double], [Double], [Double]) {
        var fftfreqs: [Double]
        var psd: [Double]
        var highFreq: [Double] = []
        var lowFreq: [Double] = []
        var veryLowFreq: [Double] = []
        
        (fftfreqs, psd) = getPSDDataViz_approx(input)
        
        for idx in 0..<fftfreqs.count {
            if (fftfreqs[idx] >= 0.0033 && fftfreqs[idx] < 0.04) {
                veryLowFreq.append(psd[idx])
            } else if (fftfreqs[idx] >= 0.04 && fftfreqs[idx] < 0.15) {
                lowFreq.append(psd[idx])
            } else if (fftfreqs[idx] >= 0.15) {
                highFreq.append(psd[idx])
            }
        }
        
        return (veryLowFreq, lowFreq, highFreq)
    }
    
    internal func getPSDDataViz_approx(_ input: [Double]) -> ([Double], [Double]) {
        let fftfreqs: [Double] = fftfreq(input.count, 1)
        let indices: [Int] = argsort(fftfreqs)
        
        var real: [Double] = []
        var im: [Double] = []
        var psd_data: [Double] = []
        var ordered_psd: [Double] = []
        
        (real, im) = dft(normalize(input))
        psd_data = get_normalized_magnitudes(real, im)
        ordered_psd = orderPSD(indices, psd_data)
        
        return (fftfreqs.sorted{return $0 < $1}, ordered_psd)
    }
}
