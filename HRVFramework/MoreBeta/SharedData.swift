//
//  SharedData.swift
//  MoreBeta
//
//  Created by Lilly Paul on 4/28/17.
//  Copyright © 2017 Cassandra Hamilton. All rights reserved.
//

import Foundation

public class SharedData{
   
   //Struct to pass all stats
   public struct statsHRV {
      public var sdnn: Double?
      public var rmssd: Double?
      public var nn50: Int?
      public var pNN50: Double?
      public var timestamp: NSDate?
      
      public var PSD: [Double]?
      public var freq: [Double]?
      public var veryLowFreq: [Double]?
      public var lowFreq: [Double]?
      public var highFreq: [Double]?
      
      public var bpm: Int?
   }
   
   //Option set to select what stats you want calculated
   public struct statsOptions: OptionSet {
      public let rawValue: Int
      public init(rawValue:Int){
         self.rawValue = rawValue
      }
      
      public static let sdnn = statsOptions(rawValue:         1 << 0)
      public static let rmssd = statsOptions(rawValue:        1 << 1)
      public static let nn50 = statsOptions(rawValue:         1 << 2)
      public static let pNN50 = statsOptions(rawValue:        1 << 3)
      public static let timestamp = statsOptions(rawValue:    1 << 4)
      public static let freqPSD = statsOptions(rawValue:      1 << 5)
      public static let freqBands = statsOptions(rawValue:    1 << 6)
      public static let bpm = statsOptions(rawValue:          1 << 7)
      
      public static let timeDomain: statsOptions = [.sdnn, .rmssd, .nn50, .pNN50, .timestamp]
      public static let freqDomain: statsOptions = [.freqPSD, .freqBands]
   }
   
   public var sharedStats = statsHRV()
   public var sharedOptions = statsOptions()
   
   internal var timeDomainMetrics = TimeDomainMetrics()
   internal var frequencyDomainMetrics = FrequencyDomainMetrics()
   
   internal func fillStatsStruct (rrIntervalsQ: Queue<Double>, bpm: Int, timestamp: NSDate) {
      //SDNN?
      if(sharedOptions.contains(.sdnn)){
         sharedStats.sdnn = (timeDomainMetrics.sdnn(rrIntervalsQ.toArray()))
      }
      else {
         sharedStats.sdnn = nil
      }
      //NN50?
      if(sharedOptions.contains(.nn50)){
         sharedStats.nn50 = timeDomainMetrics.filter_nn50(rrIntervalsQ.toArray())
      }
      else
      {
         sharedStats.nn50 = nil
      }
      //RMSSD?
      if(sharedOptions.contains(.rmssd)){
         sharedStats.rmssd = (timeDomainMetrics.rmssd(rrIntervalsQ.toArray()))
      }
      else {
         sharedStats.rmssd = nil
      }
      //PNN50?
      if(sharedOptions.contains(.pNN50)){
         sharedStats.pNN50 = timeDomainMetrics.pNN50(rrIntervalsQ.toArray())
      }
      else{
         sharedStats.pNN50 = nil
      }
      //BPM?
      if(sharedOptions.contains(.bpm)){
         sharedStats.bpm = bpm
      }
      else{
         sharedStats.bpm = nil
      }
      //Freq
      if (sharedOptions.contains(.freqPSD)) {
         var freq: [Double] = []
         var psd: [Double] = []
        
         (freq, psd) = frequencyDomainMetrics.getPSDDataViz_approx(rrIntervalsQ.toArray())

         sharedStats.freq = freq
         sharedStats.PSD = psd
      }
      else {
         sharedStats.PSD = nil
         sharedStats.freq = nil
      }
      // PSD
      if (sharedOptions.contains(.freqBands)) {
         var VLF: [Double] = []
         var LF: [Double] = []
         var HF: [Double] = []
         
         (VLF, LF, HF) = frequencyDomainMetrics.getFreqBands(rrIntervalsQ.toArray())
         
         sharedStats.veryLowFreq = VLF
         sharedStats.lowFreq = LF
         sharedStats.highFreq = HF
      }
      else {
         sharedStats.veryLowFreq = nil
         sharedStats.lowFreq = nil
         sharedStats.highFreq = nil
      }
      //Freq Domain
      if (sharedOptions.contains(.timestamp)) {
         sharedStats.timestamp = timestamp
      }
      else {
         sharedStats.timestamp = nil
      }
   }
}
