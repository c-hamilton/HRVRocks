//
//  Error.swift
//  MoreBeta
//
//  Created by Lilly Paul on 4/28/17.
//  Copyright © 2017 Cassandra Hamilton. All rights reserved.
//

import Foundation

public enum BluetoothError: Error {
   case noHRDevicesDiscoveredDueToTimeout
   case cannotGetMeticsNoDeviceConnected
   case noHeartRateMesCharacteristicFound
   case invalidDuration
}
