//
//  HRM.swift
//
//

import Foundation
import CoreBluetooth
import UIKit

public class EasyBluetooth: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
   
   // MARK: - Bluetooth GATT Services
   // Bluetooth GATT specifications - Services
   // https://developer.bluetooth.org/gatt/services/Pages/ServicesHome.aspx
   private enum BlueToothGATTServices: UInt16 {
      case BatteryService    = 0x180F
      case DeviceInformation = 0x180A
      case HeartRate         = 0x180D
      
      var UUID: CBUUID {
         return CBUUID(string: String(self.rawValue, radix: 16, uppercase: true))
      }
   }
   
   // MARK: - Bluetooth GATT Characteristics
   
   // Bluetooth GATT specifications - Characteristics
   // https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicsHome.aspx
   private enum BlueToothGATTCharacteristics: UInt16 {
      case BatteryLevel           = 0x2A19
      case BodySensorLocation     = 0x2A38
      case HeartRateMeasurement   = 0x2A37
      case ManufacturerNameString = 0x2A29
      
      func isEqual(characteristic: AnyObject) -> Bool {
         if let characteristic = characteristic as? CBCharacteristic {
            return self.UUID.isEqual(characteristic.uuid)
         }
         return false
      }
      
      var UUID: CBUUID {
         return CBUUID(string: String(self.rawValue, radix: 16, uppercase: true))
      }
   }
   
   // MARK: - Properties
   let heartRateServices = [
      BlueToothGATTServices.DeviceInformation.UUID,
      BlueToothGATTServices.HeartRate.UUID,
      BlueToothGATTServices.BatteryService.UUID
   ]
   
   
   public typealias ScanCompletionHandler = ((_ result: [EasyPeripheral]?, _ error: BluetoothError?) -> Void)
   public typealias StatCompletionHandler = ((_ error: BluetoothError?) -> Void)
   public typealias ScanProgressHandler = ((_ newDiscoveries: [EasyPeripheral]) -> Void)
   public typealias StatProgressHandler = ((_ newStats: SharedData) -> Void)
   private var scanHandlers: (progressHandler: ScanProgressHandler?, completionHandler: ScanCompletionHandler )?
   private var statHandler: (progressHandler: StatProgressHandler, completionHandler: StatCompletionHandler )?
   
   private var centralManager: CBCentralManager = CBCentralManager()
   private var hrm: HRM = HRM()
   
   private var heartRateSensorPeripheral: CBPeripheral? = nil
   private var timer: Timer? //leave uninitialized
   private var shared: SharedData?
   private var peripheralsDiscovered: [CBPeripheral] = []
   private var discoveries: [EasyPeripheral] = []
   var scanErr: BluetoothError?
   var statErr: BluetoothError?
   
   public override init() {
      super.init()
      centralManager.delegate = self
      shared = hrm.sharedData
   }
   
   // MARK: - Methods
   public func scanWithDurration(duration: TimeInterval, progressHandler: ScanProgressHandler? = nil, completionHandler: @escaping ScanCompletionHandler) {
     
      if duration <= 0 {
         scanErr = .invalidDuration
         completionHandler(nil, scanErr)
         return
      }
      
      scanHandlers = ( progressHandler: progressHandler, completionHandler: completionHandler)
      interruptScan()
      disconnectDevice()
      
      centralManager.scanForPeripherals(withServices: heartRateServices, options: nil)
      
      timer = Timer.scheduledTimer(withTimeInterval: duration, repeats: false) {_ in
         self.centralManager.stopScan()
         
         let completionHandler = self.scanHandlers?.completionHandler
         if (self.discoveries.isEmpty) {
            self.scanErr = .noHRDevicesDiscoveredDueToTimeout
         }
         self.scanHandlers = nil
         
         completionHandler?(self.discoveries, self.scanErr)
         print("scan stop")
      }
      
      print(centralManager.isScanning.description)
   }
   
   public func connectToDevice(index: Int) {
      interruptScan()
      
      centralManager.connect(peripheralsDiscovered[index], options: nil)
   }
   
   public func interruptScan() {
      centralManager.stopScan()
      timer?.invalidate()
   }
   
   public func getHRVMetricsWithDuration(duration: TimeInterval, options: SharedData.statsOptions, progressHandler: StatProgressHandler? = nil, completionHandler: @escaping StatCompletionHandler) {
       statHandler = ( progressHandler: progressHandler!, completionHandler: completionHandler)
      
      if self.heartRateSensorPeripheral?.state != .connected {
         print("Peripheral is not connected.")
         self.heartRateSensorPeripheral = nil
         statErr = .cannotGetMeticsNoDeviceConnected
         
         completionHandler(statErr)
         return
      }
      
      if duration <= 0 {
         statErr = .invalidDuration
         completionHandler(statErr)
         return
      }
      
      shared?.sharedOptions = options
      heartRateSensorPeripheral?.discoverServices([BlueToothGATTServices.HeartRate.UUID])
      
      timer = Timer.scheduledTimer(withTimeInterval: duration, repeats: false) {_ in
         
         let completionHandler = self.statHandler?.completionHandler
         self.statHandler = nil
         
         guard let services = self.heartRateSensorPeripheral?.services else {
            // disconnect directly
            self.centralManager.cancelPeripheralConnection(self.heartRateSensorPeripheral!)
            return
         }
         
         for service in services {
            // iterate through characteristics to turn off HeartRate notification
            if let characteristics = service.characteristics {
               for characteristic in characteristics {
                  if BlueToothGATTCharacteristics.HeartRateMeasurement.isEqual(characteristic: characteristic) {
                     self.heartRateSensorPeripheral?.setNotifyValue(false, for: characteristic)
                     return
                  }
               }
            }
         }

         completionHandler!(self.statErr)
         print("scan stop")
      }
      
   }
   
   public func disconnectDevice() {
      if (heartRateSensorPeripheral != nil) {
         centralManager.cancelPeripheralConnection(heartRateSensorPeripheral!)
      }
      
      peripheralsDiscovered.removeAll()
      discoveries.removeAll()
   }
   
   // MARK: - CBManagerDelegate
   public func centralManagerDidUpdateState(_ central: CBCentralManager) {
      NSLog("CBM updated")
      
      // Determine the state of the peripheral
      switch central.state {
         case CBManagerState.poweredOff:
            NSLog("CoreBluetooth BLE hardware is powered off");
            
         case CBManagerState.poweredOn:
            NSLog("CoreBluetooth BLE hardware is powered on and ready");
            
         case CBManagerState.unauthorized:
            NSLog("CoreBluetooth BLE hardware is unauthorized");
            
         case CBManagerState.resetting:
            NSLog("CoreBluetooth BLE hardware is resetting");
            
         case CBManagerState.unknown:
            NSLog("CoreBluetooth BLE hardware is unknown");
            
         case CBManagerState.unsupported:
            NSLog("CoreBluetooth BLE hardware is unsupported");
      }
   }
   
   public func centralManager(_ central: CBCentralManager,
                              didDiscover peripheral: CBPeripheral,
                              advertisementData: [String : Any],
                              rssi RSSI: NSNumber){
      print("device found!")
      if peripheralsDiscovered.contains(peripheral)
      {
         NSLog("device already discovered")
      }
      else if let name = peripheral.name
      {
         print("new device with name:", name)
         peripheralsDiscovered.append(peripheral)
         discoveries.append(EasyPeripheral(peripheral: peripheral, advertisementData: advertisementData, RSSI: Int(RSSI)))
         //call back scan progress handler
         scanHandlers?.progressHandler?(discoveries)
      }
   }
   
   public func centralManager(_ central: CBCentralManager,
                              didConnect peripheral: CBPeripheral) {
      print("Connected")
      heartRateSensorPeripheral = peripheral
      peripheral.delegate = self
   }
   
   public func centralManager(_ central: CBCentralManager,
                              didDisconnectPeripheral peripheral: CBPeripheral,
                              error: Error?) {
      heartRateSensorPeripheral = nil
      print("disconnected")
   }
   
   public func peripheral(_ peripheral: CBPeripheral,
                          didDiscoverServices error: Error?) {
      for service in peripheral.services!
      {
         print("Discovered service: \(service.uuid)")
         peripheral.discoverCharacteristics(nil, for: service)
      }
   }
   
   public func peripheral(_ peripheral: CBPeripheral,
                          didDiscoverCharacteristicsFor service: CBService,
                          error: Error?) {
      print("DID DISCOVER")
      if (service.uuid == BlueToothGATTServices.HeartRate.UUID) {
         for characteristic in service.characteristics! {
            // Request heart rate notifications
            if BlueToothGATTCharacteristics.HeartRateMeasurement.isEqual(characteristic: characteristic) {
               self.heartRateSensorPeripheral?.setNotifyValue(true, for: (characteristic ))
               print("Found heart rate measurement characteristic")
            }
         }
      }
      else {
         statErr = .noHeartRateMesCharacteristicFound
         print("No heart rate measurement characteristic found")
      }
   }
   
   
   public func peripheral(_ peripheral: CBPeripheral,
                          didUpdateValueFor characteristic: CBCharacteristic,
                          error: Error?) {
      print("DID UPDATE")
      let timestamp = NSDate()
      print("time", timestamp)
      
      switch characteristic.uuid {
      case BlueToothGATTCharacteristics.HeartRateMeasurement.UUID:
         hrm.getHeartRateMeasurementData(heartRateData:characteristic.value!, timestamp: timestamp)
         //call back stat progress handler
         statHandler?.progressHandler(shared!)
      default:
         return
      }
   }
   
}
