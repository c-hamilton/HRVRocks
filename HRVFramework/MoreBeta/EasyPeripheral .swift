//
//  EasyPeripheral .swift
//  MoreBeta
//
//  Created by Lilly Paul on 4/30/17.
//

import CoreBluetooth

public struct EasyPeripheral {
   
   // MARK: Properties
   /// The name of the device
   public let name: String?
   
   /// Setting the data advertised while the discovery was made.
   public let advertisementData: [String: Any]
   
   /// Setting the [RSSI (Received signal strength indication)](
   public let RSSI: Int
   
   // MARK: Initialization
   public init(peripheral: CBPeripheral, advertisementData: [String: Any], RSSI: Int) {
      self.advertisementData = advertisementData
      self.RSSI = RSSI
      self.name = peripheral.name
   }
   
}
