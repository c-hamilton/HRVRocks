//
//  TimeDomainMetrics.swift
//
//


import Foundation

internal class TimeDomainMetrics {
    /* Time domain metrics */
    internal func sdnn(_ data: [Double]) -> Double {
        var omegas: [Double] = [];
        
        if (data.count > 2) {
            for j in 2..<data.count {
                let slice: [Double] = Array(data[0...j]);
                let avg: Double = Double(slice.reduce(0, +)) / Double(slice.count);
                let diff: Double = Double(data[j]) - avg;
                let omega: Double = diff * diff;
                
                omegas.append(omega)
            }
        }
        
        return sqrt(omegas.reduce(0, +) / Double(data.count - 1))
    }
    
    internal func rmssd(_ data:[Double]) -> Double {
        var deltas: [Double] = []
        
        if (data.count > 2) {
            for j in 1..<data.count {
                let delta: Double = data[j] - data[j - 1]
                deltas.append(delta * delta)
            }
        }
        
        return sqrt(deltas.reduce(0, +) / Double(data.count - 1))
    }
    
    internal func filter_nn50(_ data:[Double]) -> Int {
        var count: Int = 0;
        
        if (data.count > 2) {
            for i in stride(from: 1, to: data.count, by: 1) {
                if abs(data[i] - data[i - 1]) > 50 {
                    count += 1;
                }
            }
        }
        return count;
    }
    
    internal func pNN50(_ data:[Double]) -> Double {
        let nn50_count = filter_nn50(data);
        return Double(nn50_count) / Double(data.count);
    }
}
