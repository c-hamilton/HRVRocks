//
//  HRM.swift
//
//

import Foundation
import CoreBluetooth
import UIKit

internal class HRM: NSObject{
   
   // Heart Rate Measurement flags as defined on the Bluetooth developer portal.
   // https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
   private enum HeartRateMeasurementFlags: UInt8 {
      case HeartRateValueFormat          = 0b00000001
      case ContactIsSupported            = 0b00000100
      case ContactIsSupportedAndDetected = 0b00000110
      case EnergyExpendedField           = 0b00001000
      case RRIntervalPresent             = 0b00010000
      
      func flagIsSet(flagData: UInt8) -> Bool {
         return (flagData & self.rawValue) != 0
      }
   }
   
   let samplingFreq: Double = 1024.0 //found in polar developer example, BLE rr standard
   let defaultQueueSize = 256
   let minQueueSize = 4
   
   internal var sharedData = SharedData()
   var sensorDetected = false
   var rrIntervalsQ = Queue<Double>()
   
   internal func getHeartRateMeasurementData(heartRateData: Data, timestamp: NSDate){
      var byteIndex = 0
      var hrmFlags: UInt8 = 0
      var bpm: Int = 0
      
      print("UNPACKING")
      
      heartRateData.copyBytes(to: &hrmFlags, count: MemoryLayout<UInt8>.size)
      byteIndex += MemoryLayout<UInt8>.size
      
      //UINT16 if Heart Rate Value Format flag set
      if (HeartRateMeasurementFlags.HeartRateValueFormat.flagIsSet(flagData: hrmFlags)) {
         NSLog("16UInt")
         
         var bytes: [UInt8] = [0x00, 0x00]
         let range : Range = byteIndex..<(byteIndex+MemoryLayout<UInt16>.size)
         
         heartRateData.copyBytes(to: &bytes, from: range)
         let u16 = UnsafePointer(bytes).withMemoryRebound(to: UInt16.self, capacity: 1) {
            $0.pointee
         }
         print("u16: \(u16)") // u16: 513
         
         byteIndex += MemoryLayout<UInt16>.size
         
         bpm = Int(u16)
         print("BPM =", bpm)
      }
         //UINT8 FORMAT if Heart Rate Value Format flag not set
      else {
         print("8UInt")
         var value: UInt8 = 0
         let myRange : Range = byteIndex..<(byteIndex+MemoryLayout<UInt8>.size)
         
         heartRateData.copyBytes(to: &value, from: myRange)
         byteIndex += MemoryLayout<UInt8>.size
         
         bpm  = Int(value)
         print("BPM =", bpm)
      }
      
      //Sensor Contact Status bits
      if HeartRateMeasurementFlags.ContactIsSupportedAndDetected.flagIsSet(flagData: hrmFlags) {
         print("supported and detected")
         sensorDetected = true
      }
      if HeartRateMeasurementFlags.ContactIsSupported.flagIsSet(flagData: hrmFlags) {
         print("supported")
      }
      else {
         print("not supported or detected")
      }
      
      //Energy Expended Status bit
      if HeartRateMeasurementFlags.EnergyExpendedField.flagIsSet(flagData:hrmFlags) {
         byteIndex += MemoryLayout<UInt16>.size
         print("Energy Expended Present")
      }
      
      //RR-Interval bit
      if HeartRateMeasurementFlags.RRIntervalPresent.flagIsSet(flagData: hrmFlags) {
         print("RR Interval Present")
         
         var bytes: [UInt8] = [0x00, 0x00]
         let myRangeOne : Range = byteIndex..<(byteIndex+MemoryLayout<UInt16>.size)
         var rr : Double = 0
         
         //see if multiple RR Intervals
         while byteIndex < heartRateData.count {
            print("rr interval")
            
            heartRateData.copyBytes(to: &bytes, from: myRangeOne)
            let u16 = UnsafePointer(bytes).withMemoryRebound(to: UInt16.self, capacity: 1) {
               $0.pointee
            }
            print("u16: \(u16)") // u16: 513
            
            byteIndex += MemoryLayout<UInt16>.size
            
            rr = Double(u16) / samplingFreq
            print("RR: ", rr)
            if (rrIntervalsQ.count >= defaultQueueSize) {
               _ = rrIntervalsQ.dequeue()
            }
            rrIntervalsQ.enqueue(rr)
            
         }
      }
      
      //min 4 values to do calcs
      if (rrIntervalsQ.count > minQueueSize) {
         sharedData.fillStatsStruct(rrIntervalsQ: rrIntervalsQ, bpm: bpm, timestamp: timestamp)
      }
   }
}
