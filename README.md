# README

## Abstract
Heart Rate Variability (HRV) is the physiological phenomenon of variation in the 
duration between consecutive heartbeats. HRV is a reflection of the balance between
the parasympathetic and sympathetic components of the autonomic nervous system. 
Clinical research around HRV over the last few decades have found strong correlations 
between mortality, quality of life, and disease outcomes. Unfortunately, these 
metrics have not been applied to the general population as a way to measure health.

Our Sponsor, Fullpower Technologies specializes in providing quantitative metrics 
from sensors. Current contracts include movement quantification for companies like 
Jawbone and Nike and sleep quantification and qualification. Our project aims to 
provide developers with a simple way to access heart metrics in a way that ties 
in with the current products offered by Fullpower. These quantitative insights 
can be used by developers to bring qualitative conclusions to the everyday consumer 
about their own health. HRV Analysis can be used in apps to monitor personal health, 
as well as used in studies correlating HRV to other factors. We want to empower 
the everyday user to use HRV as a tool to improve personal health.

We will be implementing an iOS framework to contain the Bluetooth connection 
logic and HRV Analysis. Our primary objective was to create data acquisition system 
to collect R-R intervals directly from Heart Straps supporting the Bluetooth HRM 
(Heart Rate Monitor) profile and use the captured R-R intervals as input to our 
HRV Analysis Engine. In order for easy adoption and continued use by developers, 
it is important that the method of distribution continues to be easy as device 
architecture changes and new features are added.

## Installing the Framework

### Follow the CocoaPods guide here:
https://guides.cocoapods.org/using/using-cocoapods.html 

### Add the dependency in the Podfile
```ruby
target 'MyApp' do
    pod 'HRVRocks', '~> 1.0'
end
```
### Installing CocoaPods
Follow the CocoaPods guide here https://guides.cocoapods.org/using/using-cocoapods.html 

## Using the Framework:
Below are the public function available to developers.

### Initialize framework:
```swift
	var ble: EasyBluetooth = EasyBluetooth()
```
### Scan for Devices:
`Function call` to scan for peripherals for 5 seconds:
```swift
	ble.scanWithDurration(duration: 5, progressHandler: { newDiscoveries in
	        // Handle newDiscoveries - a new peripheral
	    }, completionHandler: { result, error in
	        // Handle error.
	        // If no error handle result - an array of all discovered peripherals
	   });
```
`Overview` of what function does behind the scenes:
Sets up and initialized a central bluetooth in order to talk to available peripherals. 
Scan for available peripherals, filters out already discovered peripherals and 
ones not containing heart rate measurement serviced. Uses a progress handler 
callback in order to notify the user when new data is available. This allows the 
user to not be blocked waiting idly for the scan to complete. Instead, it runs in
the background and when a new device is discovered the framework alerts the app. 
This is much more effective that polling where the app waits, wasting CPU time, 
constantly polling. There is also a completion handler callback which allows the 
user to perform an action upon being notified of the scans end. It also passes
back an error variable which reports any error that occurred within the framework 
so the app can handle it cleanly. 

### Connect to Device:
`Function call` to connect to peripheral at specified index from the scan resulting array: 
```swift
	ble.connectToDevice(index: device);
```
`Overview` of what function does behind the scenes:
This function handles the centrals connection to a specified peripheral and checks 
the device actually did connect.

### Disconnect from Device:
`Function call` to disconnect from current connected peripheral: 
```swift
	ble.disconnectDevice();
```

`Overview` of what function does behind the scenes:
This function handles the centrals disconnection from the current periperal.

### Get HRV Metrics:
`Function call` to get sdnn and PSD metrics from the connected peripheral for 10 seconds.
```swift
ble.getHRVMetricsWithDuration(duration: 10, options: [.sdnn, .freqPSD], progressHandler: { stats in
        // Handle new metric
    }, completionHandler: { error in
        // Handle error
    });
```
`Overview` of what function does behind the scenes:
This function subscribes the central to the heart rate measurement services of 
the connected peripheral. After the duration, the central then unsubscribes from 
the services. This function also uses a progress handler callback in order to 
notify the user when new metrics are available. Once again, freeing up the app 
to do other stuff in the meantime. When the framework receives the new heart rate 
measurement service package, it checks to see if it contains RR values and if so
unpacks the values. It then adds these values to a queue for storage. This queue 
only keep the most recent 256 RR intervals. Then, after adding the RR interval to 
the storage, the specified metrics are then called on the stored RR intervals. For 
example, in the function call above, only the SDNN and frequency domain PSD 
calculation would be done, instead of all the metrics we offer. This is to save 
time and CPU utilization on unnecessary calculation. The resulting metrics are 
reported back to the user in a statsHRV struct as seen below. Any metric that was 
not specified has a value of NULL within the structure. The specified metrics hold 
the calculated results within the structure. The app is notified whenever there is 
a new value in the struct and can handle that data accordingly. There is also a 
completion handler callback which also passes back an error variable which reports 
any error that occurred within the framework so the app can handle it cleanly. 

```swift
public struct statsHRV {
    public var sdnn: Double?
    public var rmssd: Double?
    public var nn50: Int?
    public var pNN50: Double?
    public var timestamp: NSDate?
    public var PSD: [Double]?
    public var freq: [Double]?
    public var veryLowFreq: [Double]?
    public var lowFreq: [Double]?
    public var highFreq: [Double]?
    public var bpm: Int
    }
```

