//
//  ViewController2.swift
//  BetaSample
//

import UIKit
import MoreBetaFramework
import CorePlot

class PlotViewController: UIViewController {
    var hrv: HRVSingleton = HRVSingleton.sharedInstance
    @IBOutlet weak var hostView: CPTGraphHostingView!
    @IBOutlet weak var topHostView: CPTGraphHostingView!
    typealias plotDataType = [CPTScatterPlotField : Double]
   
   
   @IBOutlet weak var metricsButton: UIButton!
   
   @IBAction func metricsButton(_ sender: Any) {
      hrv.easyMetrics(duration: 360, options: [.sdnn, .freqPSD], topHostView: topHostView, hostView: hostView)
   }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initPlot()
    }
    
    func initPlot() {
        configureHostView()
        makeUpperGraph()
        print("configured upper")
        makeLowerGraph()
        print("configured lower")
        configureCharts()
    }
    
    func configureHostView() {
        //hostView.allowUserInteraction = true
        hostView.allowPinchScaling = true
        topHostView.allowPinchScaling = true
        
    }
    
    func setGraphTheme(for graph: CPTXYGraph) {
        //set padding for graph
        graph.paddingLeft = 0.0
        graph.paddingTop = 0.0
        graph.paddingRight = 0.0
        graph.paddingBottom = 0.0
        set_axes(for: graph)
        //graph.axisSet = nil
        
        //set padding for plotAreaFrame -- this allows for axis labels
        graph.plotAreaFrame?.masksToBorder = false;
        
        // 2 - Set up line style
        let ls = CPTMutableLineStyle()
        ls.lineColor = CPTColor.cyan()
        ls.lineWidth = 0.5
        graph.plotAreaFrame?.borderLineStyle = nil
        graph.plotAreaFrame?.cornerRadius = 0.0
        graph.plotAreaFrame?.paddingTop = 0.0
        graph.plotAreaFrame?.paddingLeft = 25.0
        graph.plotAreaFrame?.paddingBottom = 20.0
        graph.plotAreaFrame?.paddingRight = 4.0
        
        //Create text style for titles
        let textStyle: CPTMutableTextStyle = CPTMutableTextStyle()
        textStyle.color = CPTColor.white()
        textStyle.fontName = "HelveticaNeue"
        textStyle.fontSize = 16.0
        textStyle.textAlignment = .center
        //Set graph title style
        graph.titleTextStyle = textStyle
        graph.titlePlotAreaFrameAnchor = CPTRectAnchor.top
        
        // Put an colorful area gradient
        let areaColor    = CPTColor(componentRed: 0.3, green: 0.0, blue: 1.0, alpha: 0.8)
        let areaGradient = CPTGradient(beginning: areaColor, ending: CPTColor.clear())
        areaGradient.angle = -90.0
        let areaGradientFill = CPTFill(gradient: areaGradient)
        graph.fill = areaGradientFill
    }
    
    
    func set_axes(for graph: CPTXYGraph){
        // Axes
        let axisSet = graph.axisSet as! CPTXYAxisSet
        let plotSpace = graph.allPlotSpaces().last as! CPTXYPlotSpace
        
        // 1 - Configure styles
        let axisLineStyle = CPTMutableLineStyle()
        axisLineStyle.lineWidth = 1.0
        axisLineStyle.lineColor = CPTColor.black()
        
        if let x = axisSet.xAxis {
            x.title = "x-axis" //need better logic here to get title
            x.labelingPolicy = .automatic
            x.labelOffset = -4
            x.axisConstraints = CPTConstraints.constraint(withLowerOffset: 0.0)
            x.preferredNumberOfMajorTicks = 5
            
            
            let formatter = NumberFormatter()
            formatter.usesSignificantDigits = true
            x.labelFormatter = formatter
            
            /* SOME OTHER CORE PLOT CUSTOM CONFIGS
            x.axisConstraints = CPTConstraints.constraint(withLowerOffset: 0.0)
            x.majorIntervalLength   = 0.5
            x.orthogonalPosition    = 2.0
            x.minorTicksPerInterval = 2
            x.visibleAxisRange = plotSpace.xRange
            //
            x.labelOffset = -4
            x.tickDirection = .negative
            x.labelFormatter = CPTTimeFormatter();
            */
        }
        if let y = axisSet.yAxis {
            y.labelingPolicy = .automatic
            y.labelOffset = -4
            y.axisConstraints = CPTConstraints.constraint(withLowerOffset: 0.0)
            let formatter = NumberFormatter()
            formatter.usesSignificantDigits = true
            y.labelFormatter = formatter
        }
        
        //TODO test this method by calling it from ViewController
        //updateYRange(for: graph)
    }
    
    public func updateYRange(for graph: CPTXYGraph, for yArray: Array<Double>? = nil, for xArray: Array<Double>? = nil){
        let plotSpace = graph.allPlotSpaces().last as! CPTXYPlotSpace
        guard let axisSet = graph.axisSet as? CPTXYAxisSet else { return }
        
        
        guard let y = axisSet.yAxis else { return }
        //pretty sure about this? location and length is wierd naming
        
        /* http://stackoverflow.com/questions/24570921/core-plot-change-y-axis-labels-and-scale-to-plot-a-new-graph
         self.plotSpace.globalYRange = nil;
         self.plotSpace.yRange = newRange;
         self.plotSpace.globalYRange = self.plotSpace.yRange;
         */
        
        let yRange = ["min" : y.visibleAxisRange!.locationDouble, "max": y.visibleAxisRange!.lengthDouble] as [String : Double]
        let yMin = yArray?.min() //TODO  implement a yRecentMin
        let yMax = yArray?.max()
        
        if (yArray == nil){return}
        if Double(yRange["max"]!) < yMax!{
            let newYRange = CPTMutablePlotRange(location: NSNumber(value: yMin!), length: NSNumber(value: yMax!))
            CPTAnimation.animate(plotSpace, property: "yRange", from: y.visibleAxisRange!, to: newYRange, duration: 0.25)
        }
        
        if (xArray == nil){return}
        guard let x = axisSet.xAxis else { return }
        let xRange = ["min" : (x.visibleAxisRange!.locationDouble), "max": x.visibleAxisRange!.lengthDouble] as [String : Double]
        let xMin = xArray?.min() //TODO  Support scaling with recent maxmin
        let xMax = xArray?.max()
        if Double(xRange["max"]!) < xMax!{
            print("xMin \(String(describing: xMin)) ")
        }
    }
    
    
    func makeUpperGraph() {
        let graph = CPTXYGraph(frame: topHostView.bounds)
        topHostView.hostedGraph = graph
        setGraphTheme(for: graph)
        graph.title = "Frequency Domain"
        
        guard let plotSpace = graph.defaultPlotSpace as? CPTXYPlotSpace else { return }
        plotSpace.xRange = CPTPlotRange(locationDecimal: 0.0, lengthDecimal: 0.7)
        plotSpace.yRange = CPTPlotRange(locationDecimal: 0, lengthDecimal: 1e-2)
        plotSpace.allowsUserInteraction = true;
        plotSpace.allowsMomentumX = true;
    }
    
    func makeLowerGraph() { //bottom graph
        let graph = CPTXYGraph(frame: hostView.bounds)
        hostView.hostedGraph = graph
        setGraphTheme(for: graph)
        graph.title = "Time Domain"
        
        guard let plotSpace = graph.defaultPlotSpace as? CPTXYPlotSpace else { return }
        plotSpace.allowsUserInteraction = true;
        plotSpace.allowsMomentumX = true;
        plotSpace.xRange = CPTPlotRange(locationDecimal: 0, lengthDecimal: 30)
        plotSpace.yRange = CPTPlotRange(locationDecimal: 0, lengthDecimal: 1)
    }

    func configureCharts() { //top graph
        // 1 - Get a reference to the graph
        let graph = hostView.hostedGraph!
        let newGraph = topHostView.hostedGraph!
        
        // Create bottom plot
        let plot = CPTScatterPlot()
        plot.delegate = self
        plot.dataSource = self.hrv //see line 122
        plot.identifier = NSString(string: graph.title!)
        
        // Create top plot
        let dataSourceLinePlot = CPTScatterPlot(frame: .zero)
        dataSourceLinePlot.delegate = self
        dataSourceLinePlot.dataSource = self.hrv //see line 122
        dataSourceLinePlot.identifier    = NSString.init(string: newGraph.title!)
        
        //        If you want lables on the data
        //        let textStyle = CPTMutableTextStyle()
        //        textStyle.color = CPTColor.cyan()
        //        textStyle.textAlignment = .center
        //        plot.labelTextStyle = textStyle
        
        
        graph.add(plot, to: graph.defaultPlotSpace)
        // Animate in the new plot, as an example
        dataSourceLinePlot.opacity = 0.0
        newGraph.add(dataSourceLinePlot, to: newGraph.defaultPlotSpace)
        
        let fadeInAnimation = CABasicAnimation(keyPath: "opacity")
        fadeInAnimation.duration            = 2.0
        fadeInAnimation.isRemovedOnCompletion = false
        fadeInAnimation.fillMode            = kCAFillModeForwards
        fadeInAnimation.toValue             = 1.0
        dataSourceLinePlot.add(fadeInAnimation, forKey: "animateOpacity")
    }
    
    
    //TODO: can we dispose of HR data here?
    //      or can we assume that a sample app doesnt need this?
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
}

extension PlotViewController: CPTScatterPlotDelegate {
}
