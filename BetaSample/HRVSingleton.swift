//
//  HRVSingleton.swift
//  BetaSample
//
//  Created by Cassandra Hamilton on 5/14/17.
//  Copyright © 2017 Cassandra Hamilton. All rights reserved.
//

import UIKit
import CorePlot
import MoreBetaFramework

class HRVSingleton: NSObject {
   var ble: EasyBluetooth?
   public var discoveries: [EasyPeripheral] = []
   public var sharedSDNN = 0.0 as Double
   public var sdnn: Array<Double> = [0.075]
   public var freq: Array<Double> = [0.1]
   public var psd: Array<Double> = [0.01]
   
   //todo make "dummy data" arrays which will be returned if the
   class var sharedInstance: HRVSingleton {
      struct Singleton {
         static let instance = HRVSingleton()
      }
      return Singleton.instance
   }
   
   override init(){
      self.ble = EasyBluetooth()
      super.init()
   }
   
   func easyScan(duration: Int, tableView: UITableView) {
      discoveries.removeAll()
      tableView.reloadData()
      
      ble?.scanWithDurration(duration: 5, progressHandler: { newDiscoveries in
         // Handle newDiscoveries, [BKDiscovery].
         print("ADDED ", newDiscoveries.last?.name ?? "NULL")
         self.discoveries = newDiscoveries
         tableView.reloadData()
      }, completionHandler: { result, error in
         // Handle error.
         // If no error, handle result, [BKDiscovery].
         self.discoveries = result!
         print("error?", error ?? "none")
         print("Done!")
      })
   }
   
   func easyConnect(device: Int) {
       ble?.connectToDevice(index: device)
   }
   
   func easyMetrics(duration: Int, options: SharedData.statsOptions, topHostView: CPTGraphHostingView, hostView: CPTGraphHostingView) {

      ble?.getHRVMetricsWithDuration(duration: 360, options: options, progressHandler: { stats in
         // TODO is this a fair assumption?
         if (stats.sharedStats.sdnn != nil) {
            print("SDNN", stats.sharedStats.sdnn!)
            //TODO how do we manage memory?
            self.sdnn.insert(stats.sharedStats.sdnn!, at: self.sdnn.count)
            self.freq = stats.sharedStats.freq!
            self.psd = stats.sharedStats.PSD!
           
            hostView.hostedGraph?.reloadData()
            topHostView.hostedGraph?.reloadData()
            
         }
      }, completionHandler: { error in
         // Handle error.
         print("Metric error?", error ?? "none")
         print("Metric Scan Done!")
      })
   }
}

extension HRVSingleton: UITableViewDataSource{
   public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return discoveries.count //how many rows in table
   }
   
   public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell : UITableViewCell = UITableViewCell()
      cell.textLabel?.text = discoveries[indexPath.row].name
      cell.backgroundView?.backgroundColor = UIColor(colorLiteralRed: 255, green: 0, blue: 0, alpha: 0)
      return cell
   }
   
}

extension HRVSingleton: CPTScatterPlotDataSource{
   func numberOfRecords(for plot: CPTPlot) -> UInt{
      let plotID = plot.identifier as! String
      
      if (plotID == "Time Domain"){
         return UInt(sdnn.count)
      }else{
         return UInt(freq.count)
      }
   }
   
   func number(for plot: CPTPlot, field fieldEnum: UInt, record idx: UInt) -> Any? {
      let plotID = plot.identifier as! String
      switch CPTScatterPlotField(rawValue: Int(fieldEnum))! {
      case .X:
         
         if (plotID == "Time Domain"){
            return idx as NSNumber //can also return ts[idx]
         }else{
            return freq[Int(idx) ] as NSNumber
         }
      case .Y:
         
         if (plotID == "Time Domain"){
            return sdnn[Int(idx)] as NSNumber
         }else{
            return psd[Int(idx) ] as NSNumber
         }
         
      }
   }
}
