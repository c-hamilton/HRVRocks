//
//  ViewController.swift
//
import UIKit
import MoreBetaFramework// our framework

class ScanViewController: UIViewController, UITableViewDelegate{
    var table: UITableView?
    var hrv = HRVSingleton.sharedInstance
   
    @IBOutlet weak var deviceTable: UITableView!
    @IBOutlet weak var connectedLabel: UILabel!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var ourName: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        table = self.deviceTable
        deviceTable.dataSource = hrv
        deviceTable.delegate = self
        scanButton.layer.cornerRadius = 5
    }
    
    @IBAction func scanButton(_ sender: UIButton) {
      hrv.easyScan(duration: 5, tableView: deviceTable)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
   func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
      if(hrv.discoveries.count != 0) {
         print("highlighted rowww! ", hrv.discoveries[indexPath.row].name! )
         hrv.easyConnect(device: indexPath.row)
         self.connectedLabel.text = "Device Connected!🤘"
         tableView.reloadData()
   }
  }
}
